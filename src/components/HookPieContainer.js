import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { buyPie } from '../redux'

function HookPieContainer() {
    const numOfPies = useSelector(state => state.pie.numOfPies)
    const dispatch = useDispatch()
    return (
        <div>
            <h2>Num of pies - {numOfPies}</h2>
            <button onClick={() => dispatch(buyPie())}>Buy Pie</button>
        </div>
    )
}

export default HookPieContainer
